# Windows HTTP Proxy

Manage the HTTP proxy settings within Microsoft Windows.

## Table of Contents

1. [Description](#description)
2. [Usage - Configuration options and additional functionality](#usage)
3. [Limitations - OS compatibility, etc.](#limitations)
4. [Development - Guide for contributing to the module](#development)

## Description

This module assists with managing the HTTP proxy configuration settings on a Windows node. It leverages PowerShell to do the bulk of the heavy lifting. By default, it will not make any modifications (to ensure unintentional breakage does not happen on a node).

## Usage

To use the module, supply the desired configuration:
```
  windows_http_proxy {
    bypass_list => '<local>;*.test.sandbox.company.com;*.company.com;',
    ensure      => 'present',
    port        => 9191,
    server      => 'ourproxyserver.company.com',
  }
```

When troubleshooting a node issue, create a System Environment variable with a Name of "PUPPET_PS_HELPER_DEBUG" and a Value of "True". Then trigger a Puppet agent run; after which you may review the contents of the "C:\Windows\Temp\windows_http_proxy.log" file.

## Limitations

1. This module uses the following to generate the contents of the REFERENCE.md file:
  * `puppet strings generate --format markdown --out REFERENCE.md`
2. Command to apply this module locally (and kick the tires):
`puppet apply --modulepath="<PathToModuleParentFolders>" --execute "include <ModuleName>" --environment "<EnvironmentName>" --no-splay --verbose --debug`
  * For example: `puppet apply --modulepath="C:\ProgramData\PuppetLabs\code\environments\production\modules;c:\projects\forge" --execute "include windows_http_proxy" --environment "production" --no-splay`

## Development

Feedback and ideas are always welcome - please contact an Author (listed in metadata.json) to discuss your input, or feel free to simply [open an Issue](https://gitlab.com/mgeppetto/windows_http_proxy/-/issues).

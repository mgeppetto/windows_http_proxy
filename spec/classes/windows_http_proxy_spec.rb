require 'spec_helper'

describe 'windows_http_proxy' do
  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }

      context 'with acceptable parameters' do
        let(:title) { 'init_windows_http_proxy' }
        let(:params) do
          {
            'bypass_list' => '<local>;server101.company.com;server202.company.com;',
            'ensure'      => 'present',
            'log_output'  => false,
            'port'        => 9292,
            'server'      => 'secureproxy.company.com',
          }
        end

        it { is_expected.to compile }
        it { is_expected.to compile.with_all_deps }
        it { is_expected.to contain_class('windows_http_proxy::config') }
      end

      describe 'windows_http_proxy::config' do
        # private class
        let(:title) { 'config_windows_http_proxy' }
        let(:params) do
          {
            'bypass_list' => '<local>;*.company.com;',
            'ensure'      => 'present',
            'log_output'  => false,
            'port'        => 8282,
            'server'      => 'proxy.company.com',
          }
        end

        it { is_expected.to compile }
        it { is_expected.to contain_exec("Set 'windows_http_proxy' HTTP proxy for 'proxy.company.com'") }
      end # windows_http_proxy::config
    end
  end
end

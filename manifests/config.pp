# @summary Configuration
#
# Configures Microsoft Windows HTTP proxy
#
# @api private
#
# @example
#   include windows_http_proxy::config
class windows_http_proxy::config {
  assert_private()
  if ($windows_http_proxy::ensure != 'unmanaged') {
    if ($windows_http_proxy::ensure == 'present') {
      if (($windows_http_proxy::port == undef) or ($windows_http_proxy::port == 0)) {
        fail 'Invalid port'
      }
      if (($windows_http_proxy::server == undef) or ($windows_http_proxy::server == '')) {
        fail 'Invalid server'
      }
    }
    exec { "Set '${module_name}' HTTP proxy for '${windows_http_proxy::server}'":
      provider  => powershell,
      command   => template(
        "${module_name}/helpers.ps1.erb",
        "${module_name}/proxy_command.ps1.erb"
      ),
      onlyif    => template(
        "${module_name}/helpers.ps1.erb",
        "${module_name}/proxy_onlyif.ps1.erb"
      ),
      logoutput => $windows_http_proxy::log_output,
    }
  }
}

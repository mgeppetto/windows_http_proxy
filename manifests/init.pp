# @summary Windows HTTP Proxy
#
# Manages Microsoft Windows HTTP proxy
#
# @param bypass_list
#   String containing the list of addresses that should not go through the proxy
#   Example: '<local>;*.sandbox.company.com;*.company.com;'
#
# @param ensure
#   denotes the desired state of the supplied proxy server
#   Example: 'present'
#
# @param log_output
#   Boolean used to enable additional logging output
#   Example: true
#
# @param port
#   Denotes the port to use on the proxy server
#   Example: 8282
#
# @param server
#   String containing the FQDN of the proxy server to use
#   Example: 'proxy.company.com'
#
# @example
#   include windows_http_proxy
class windows_http_proxy (
  String $bypass_list = '<local>',
  Enum['absent', 'present', 'unmanaged'] $ensure = 'unmanaged',
  Optional[Integer] $port = undef,
  Optional[String] $server = undef,
  Boolean $log_output = false,
) {
  if ($log_output) {
    notify { "${module_name} - bypass_list: '${bypass_list}'": }
    notify { "${module_name} - ensure: '${ensure}'": }
    notify { "${module_name} - port: '${port}'": }
    notify { "${module_name} - server: '${server}'": }
  }
  include windows_http_proxy::config
}
